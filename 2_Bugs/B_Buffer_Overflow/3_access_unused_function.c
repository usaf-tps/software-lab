#include <stdio.h>
#include <string.h>

void unused(void) {
  printf("This code should never run\n");
}

int main(int argc, char *argv[]) {
  if (argc==2){
    char msg[64] = "hello";
    char buf[64];

    printf("input string length = %d\n", strlen(argv[1]));
  
    // copy the input to the buffer (one we are overflowing)
    strcpy(buf, argv[1]);

    // print the msg buffer to the screen
    printf("The message is: %s \n", msg);
    printf("The input buffer is: %s \n", buf);
  } else {
    printf("Error: Forgot to give input");
  }

  return 0;
}
