#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>

// the password
const char *PASSWORD_FILE = "passTPS";

int main()
{
  // our 2 buffers (each holds 8 characters)
  char password[8];
  std::string input;
  
  // read the password into the password buffer
  std::sscanf(PASSWORD_FILE, "%s", password);

  // get an input from the user, store in the input buffer
  std::cout << "Enter password: ";
  std::cin >> input;

  // Debug prints:
  std::cout << "Address of input   : " << &input << "\n";
  std::cout << "Address of password: " << &password << "\n";
  std::cout << "Input: " << input << "\n";
  std::cout << "Password: " << password << "\n";

  // if first 8 characters of input match password, pass, else deny
  if (std::strncmp(password, input.c_str(), 8) == 0)
    std::cout << "Access granted\n";
  else
    std::cout << "Access denied\n";

  return 0;
}