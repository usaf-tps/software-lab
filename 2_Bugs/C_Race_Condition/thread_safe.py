import concurrent.futures
import threading
import time

class FakeDatabase:
    def __init__(self):
        self.value = 0
        self._lock = threading.Lock()

    def update(self, val):
        with self._lock:
            print("[read] Thread #: " + str(val) +
                  " reading current value: " + str(self.value))
            local_copy = self.value
            local_copy += 1
            print("[write] Thread #: " + str(val) +
                  " updating current value to: " + str(local_copy))
            self.value = local_copy

if __name__ == "__main__":
    database = FakeDatabase()
    print("Thread safe, no race condition")
    print("initial database value = " + str(database.value))
    start_time = time.time()
    with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
        for index in range(100):
            print("starting worker #: " + str(index))
            future = executor.submit(database.update, index)
    stop_time = time.time()
    future.result()
    print("predicted final database value = " + str(index + 1))
    print("final database value = " + str(database.value))
    print("total time = " + str(stop_time-start_time) + " seconds")
