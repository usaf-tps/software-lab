#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  if (argc==2){
    short alt = 0;
    char *input = argv[1];
    alt = atoi(input) * 1;

    if(alt > 0) {
      printf("altitude is positive \n");
    } else {
      printf("altitude is negative \n");
    }

    printf("variable alt, stored as = %d \n", alt);
  } else {
    printf("Error: Forgot to give input");
  }

  return 0;
}
