# Software_Lab

This Repo contains files to support the USAF TPS Computer Software and Networks Laboratory (part of SY7221). The goal is to provide hands-on experience with: software languages, software bugs, CI, virutalization. 
As such, the files are organized into folders for each DLO.

## 1. Computer Languages (Compiled vs. Interpreted)
Compare Complied language (e.g., C/C++) to Interpreted langauge (e.g., python).
- `alt2temp.{cpp, py}`: same function in C++ and Python
- (Future) Comparison of Matrix Multiplication across languages (C++, Rust, Julia, Java, Python)

## 2. Software Bugs
Gain familiarity with common software bugs. Specifically: 
- (A) Integer Overflow 
- (B) Buffer Overflow  
- (C) Race Condition   

## 3. Automated Testing & Continuous Integration (CI)
Gain familiarity with automated test tools, specifically:
- (A) Lint 
- (B) Unit test

Push code changes and observe CI pipeline performing these tasks.

## 4. Virtualization
Limited code here. Compare startup times for VMs and Containers. Run VMs using VirtualBox and Containers using Docker Desktop.

Example of "containerizing code." Where `alt2temp.py` from Section 1 is placed into a Docker Container.
- (B) Containers

## (Future) 5. Internet Protocol (IP) Networks 
Gain familiarity with IP data transfer, specifically:
- (A) User Datagram Protocol (UDP)
- (B) Transmission Control Protocol (TCP)

## (Future) 6. Graphics Processing Unit (GPU)
Gain familiarity with differences in GPU performance, compared to CPU