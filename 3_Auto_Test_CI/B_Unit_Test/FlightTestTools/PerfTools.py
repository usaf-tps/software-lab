
def alt2temp(h):
    '''
    Calculate temperature in R
    Input:
        h : pressure altitude in ft
    Output:
        T : temperature
    '''
    def calcT(h):
        if h > 36089:
            T = 390
        else:
            T = To * (1-6.87559e-6 * h)
        return T

    To = 518.69

    if isinstance(h,list):
        T=[]
        for item in h:
            T.append(calcT(item))
    else:
        T = calcT(h)

    return T


def MachAlt2TAS(M, h, dT):
    '''
    Calculate true airspeed given Mach and altitude
    Inputs:
        M : Mach number
        h : Pressure altitude in ft
        dT: Temperature difference from standard day in F or R
    Output:
        vt: True airspeed in ft/sec
    '''
    gamma = 1.4 # ratio of specific heats
    R = 1716.59 # Gas constant
    T = alt2temp(h) + dT
    a = (gamma * R * T) ** (1/2)
    vt = M * a

    return vt
