import unittest
from FlightTestTools import PerfTools

class TestAlt2Temp(unittest.TestCase):
    def test_alt2temp(self):
        h = [0, 36089, 36090, 40000]
        Tout = []
        for i in h:
            Tout.append(PerfTools.alt2temp(i))
        self.assertEqual(Tout[2], Tout[3], 390)
        self.assertAlmostEqual(Tout[1], 389.985, delta=0.025)
        self.assertAlmostEqual(Tout[0], 518.69, delta=0.025)


    def test_alt2temp_vector(self):
        h = [0, 36089, 36090, 40000]
        Tout = PerfTools.alt2temp(h)
        self.assertEqual(Tout[2], Tout[3], 390)
        self.assertAlmostEqual(Tout[1], 389.985, delta=0.025)
        self.assertAlmostEqual(Tout[0], 518.69, delta=0.025)


class TestMachAlt2TAS(unittest.TestCase):
    def test_MachAlt2TAS(self):
        M = 0.5
        h = 10000
        dT = 50
        Tout = PerfTools.MachAlt2TAS(M,h,dT)
        self.assertAlmostEqual(Tout, 565.90, delta=.025)
