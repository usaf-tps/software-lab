"""
An example script to demonstrate linting 
"""
import math
import numpy as np

class demo_class:
    """A class, part of object oriented programming"""
    def aDd_NeW_vars(self, value=42):
        """A function within the class"""
        temp = np.random.rand()
        self.value = value

    def show_value(self):
        print(self.value)


def add2items(item_A, ItemB):
    """A function that adds 2 items"""
    return float(item_A) + float(ItemB)

TPS_class = demo_class()
TPS_class.aDd_NeW_vars()
TPS_class.show_value()

Val_a = 1.0
Val_b = '2'
Out_c = add2items(Val_a, Val_b)
print(Out_c)
