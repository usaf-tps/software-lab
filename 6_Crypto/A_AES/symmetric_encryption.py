from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

# Message
data = b'secret data'

print("Initial input data: " + str(data))

# Encryption Key
key = get_random_bytes(16)

print("Encryption key: " + str(key))

# AES Encryption, Message to Ciphertext
cipher = AES.new(key, AES.MODE_GCM)
ciphertext, tag = cipher.encrypt_and_digest(data)

print("Encrypted using AES: " + str(ciphertext))

# Decrypt message, Ciphertext to Message
nonce = cipher.nonce
cipher = AES.new(key, AES.MODE_GCM, nonce)
data = cipher.decrypt_and_verify(ciphertext, tag)

print("Decrypted using same key: " + str(data))
