import rsa

# Message
message = 'secret data'

print("Initial input data: " + message)

# 2 Keys: Public & Private Key
(publicKey, privateKey) = rsa.newkeys(512)

print("Public Key, used for Encrypting Message: " + str(publicKey.e))

# Encrypt Message using Public Key
ciphertext = rsa.encrypt(message.encode('ascii'), publicKey)

print("Encrypted using RSA & Public Key:" + str(ciphertext))

# Decrypt Message using Private Key
print("Private Key, used for Decrypting Message: " + str(privateKey.d))

data = rsa.decrypt(ciphertext, privateKey).decode('ascii')

print("Decrypted using RSA & Private Key: " + data)
