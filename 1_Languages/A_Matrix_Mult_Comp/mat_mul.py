import timeit
import numpy as np

MATRIX_SIZE = 64

A = np.random.rand(MATRIX_SIZE, MATRIX_SIZE)
B = np.random.rand(MATRIX_SIZE, MATRIX_SIZE)

t = timeit.repeat(lambda: A*B, repeat=10, number=5)
print(t)
