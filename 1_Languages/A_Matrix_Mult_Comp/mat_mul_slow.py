import timeit
import numpy as np

MATRIX_SIZE = 64

A = np.random.rand(MATRIX_SIZE, MATRIX_SIZE)
B = np.random.rand(MATRIX_SIZE, MATRIX_SIZE)
C = np.zeros((MATRIX_SIZE, MATRIX_SIZE))

def mult(a,b,n):
    c = np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            for k in range(n):
                c[i][j] += a[i][k] * b[k][j]
    return c

t = timeit.repeat(lambda: mult(A,B, MATRIX_SIZE), repeat=10, number=5)
print(t)
