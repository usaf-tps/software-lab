'''Client to send TCP data'''
import socket
import time

def client_program():
    host = '172.42.42.2' #IP address assigned to server
    port = 5000  # socket server port number

    client_socket = socket.socket()  # instantiate
    client_socket.connect((host, port))  # connect to the server
    print("TCP Client connected to Server")

    with open("data.txt", "r", encoding='utf-8') as f:
        messages = f.readlines()
    messages = [item.replace("\n", " ") for item in messages]

    start_time = time.time()
    for message in messages:
        print("sending "+ message)
        client_socket.send(message.encode())  # send message

    stop_time = time.time()
    print("TCP Transmission Complete, connection closed")
    print(f"[TCP] Time to send = {(stop_time-start_time):.2e} seconds")

    # Give time for TCP retransmits before closing
    time.sleep(1)
    client_socket.close()  # close the connection


if __name__ == '__main__':
    client_program()
