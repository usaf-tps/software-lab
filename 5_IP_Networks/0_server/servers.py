'''server for receiving tcp data'''
import socket
from multiprocessing import Process
import time


def tcp_server():
    host = '172.42.42.2'
    port = 5000  # initiate port above 1024

    server_socket = socket.socket()
    server_socket.bind((host, port))  # bind host address and port together

    # configure how many client the server can listen simultaneously
    server_socket.listen(2)
    while True:
        conn, address = server_socket.accept()  # accept new connection
        while True:
            # receive data. it won't accept data packet greater than 1024 bytes
            data = conn.recv(1024).decode()
            if not data:
                # if data is not received break
                break
            print("[TCP] Received: " + str(data))
        conn.close()


def udp_server():
    host = '172.42.42.2'
    port = 6000  # initiate port no above 1024

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind((host, port))  # bind host address and port together
    while True:
        # receive data. it won't accept data packet greater than 1024 bytes
        data,_ = server_socket.recvfrom(1024)
        if not data.decode():
            # if data is not received break
            break
        print("[UDP] Received: " + str(data.decode()))


if __name__ == '__main__':
    print("TCP & UDP Server Started")
    p1 = Process(target=udp_server).start()
    p2 = Process(target=tcp_server).start()
    while True:
        pass
