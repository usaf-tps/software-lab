#!/bin/bash

if [ ! -z "$delay" ]; then
    tc qdisc add dev eth0 root netem delay 10ms 10ms 25%
fi

python3 udp_client.py
