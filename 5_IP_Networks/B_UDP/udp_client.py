'''Client to send UDP data'''
import socket
import time

def client_program():
    '''
    host = socket.gethostname()  # as both code is running on same pc
    '''
    host = '172.42.42.2'
    port = 6000  # socket server port number

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # instantiate
    client_socket.connect((host, port))  # connect to the server
    print("UDP Client connected to Server")

    with open("data.txt", "r", encoding='utf-8') as f:
        messages = f.readlines()
    messages = [item.replace("\n", " ") for item in messages]

    start_time = time.time()
    for message in messages:
        print("sending "+ message)
        client_socket.send(message.encode())  # send message

    stop_time = time.time()
    print("UDP Transmission Complete, connection closed")
    print(f"[UDP] Time to send = {(stop_time-start_time):.2e} seconds")
    client_socket.close()  # close the connection


if __name__ == '__main__':
    client_program()
